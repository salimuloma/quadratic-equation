//import the library for the c language
#include <stdio.h>
//import the library for maths calculations
#include <math.h>

//this program solves the quadratic equations
//ax^2+bx


/*
a:this is the coeffiecient for the squared variable
b: this is the coefficient for the linear variable
c: this is the constant of the equation
*/

int main(){
    //assign the variables to be used ie a,b &c
    int a,b,c;

    //prompt for the user inputs of a,b and c

    printf("Enter the coefficient of the squared variable,a:\n ");
    scanf("%d",&a);

    //validate the 'a' input such that it is not 0

    if (a==0){
        printf("this cannot be a quadraic equation\nYou have to input a value greater to make it valid!\n");
        
    }else{
        printf("Enter the value for the linear variable,b:\n");
        scanf("%d",&b);

        printf("enter the value of the constant,c:\n");
        scanf("%d",&c);

        //determine the discriminant of the equation
        //b^2-(4*a*c)

        int discriminant;
        discriminant=((b*b)-(4*a*c));

        //validate the discriminant to know the type of roots

        if (discriminant>0){
            //real and distinct roots                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
            printf("the roots are real and distinct!\n");
            double root1=(-b+(sqrt(discriminant)))/(2.0*a);
            double root2=(-b-(sqrt(discriminant)))/(2.0*a);
            printf("root1= %.4f\n",root1);
            printf("root2= %.4f\n",root2);
        }else if(discriminant==0){
            //real and similar roots
            printf("the roots are real and similar\n");
            double root=-b/(2*a);
            printf("root= %.4f\n",root);
        }else{
            //complex roots
            printf("the roots are real and imaginary!\n");
            double realPart=-b/(2.0*a);
            double imaginaryPart=sqrt(-discriminant)/(2.0*a);
            printf("the real part solves to: %.4f\n",realPart);
            printf("the imaginary part solves to: %.4fi\n",imaginaryPart);
            printf("real root= %.4f+ %.4fi\n",realPart,imaginaryPart);
            printf("imaginary root= %.4f -%.4fi\n",realPart,imaginaryPart);
        }
    }


    return 0;
}